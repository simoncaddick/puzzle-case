#include <Wire.h>

// I2C States
byte aliveState = 5;
byte activeState = 6;
byte failState = 7;
byte successState = 8;
byte activateState = 2;

byte currentState = 0;

// Module pins
int buttonFailPin = 10;
int buttonSuccessPin = 11;
int buttonActivatePin = 2;

// Mode params
bool ready = false;

void setup() { 
//I2C stuff
  Wire.begin(10);
  Wire.onRequest(requestEvent);
  

  pinMode(buttonActivatePin, INPUT_PULLUP);
  pinMode(buttonFailPin, INPUT);
  pinMode(buttonSuccessPin, INPUT);

  currentState = aliveState;

}

void requestEvent(){
  Wire.write(currentState);
}

void loop() {
  // If we have been activated
  if (currentState == activeState) {
    // Check correct button has been pressed and return result to master
    if (digitalRead(buttonFailPin) == HIGH)
    {
      digitalWrite(failPin, HIGH);
    }
    else if (digitalRead(buttonSuccessPin == HIGH) {
      digitalWrite(successPin, HIGH);
    }
    else {
      digitalWrite(failPin, LOW);
      digitalWrite(successPin, LOW);
    }
  }
}
