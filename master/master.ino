// Program: I2C master reader template for multi-node Arduino I2C network
// Programmer: Hazim Bitar (techbitar.com)
// Date: March 30, 2014
// This example code is in the public domain.

#include <Wire.h>

#define PAYLOAD_SIZE 3 // how many bytes to expect from each I2C salve node
#define NODE_MAX 24 // maximum number of slave nodes (I2C addresses) to probe
#define START_NODE 2 // The starting I2C address of slave nodes
#define NODE_READ_DELAY 500 // Some delay between I2C node reads

int lives, modules, difficulty, time, availableNodeCount;
bool ready, start, booted, reset;
int availableNodes[NODE_MAX], activeNodes[6];  // List of available and active nodes
int nodePayload[PAYLOAD_SIZE];
int startInterruptPin = 2;

void setup()
{
  // Serial Debugging
  Serial.begin(9600);  
  Serial.println("MASTER READER NODE");
  Serial.print("Maximum Slave Nodes: ");
  Serial.println(NODE_MAX);
  Serial.print("Payload size: ");
  Serial.println(PAYLOAD_SIZE);
  Serial.println("***********************");
  

  booted = false;
  ready = false;
  start = false;
  reset = false;
  availableNodeCount = 0;

  // Temporary difficulty 
  difficulty = 1;
  
  pinMode(startInterruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(startInterruptPin), isr, RISING);
  Wire.begin();        // Activate I2C link
}

void loop()
{
  if(ready && !start)
  {
    Serial.println("Start");
    prepare();
    ready = false;
    start = true;
    Serial.println("****Prepared****");
  }
  if(!ready && start)
  {
    checkModules();
  }

  if(!booted)
  {
    delay(5000);
    booted = true;
  }
}

void checkModules()
{
  Serial.println("Begin check modules");
  Serial.println("Available Node count:");  
  Serial.println(availableNodeCount);    
  for (int i = 0; i < availableNodeCount; i++) 
  { 
    Wire.requestFrom(activeNodes[i], PAYLOAD_SIZE);    // request data from node#
    if(Wire.available() == PAYLOAD_SIZE)            // if data size is avaliable from nodes
    {
      for (int j = 0; j < PAYLOAD_SIZE; j++)        // get nodes data
      {
        nodePayload[j] = Wire.read(); 
      }  
      for (int k = 0; k < PAYLOAD_SIZE; k++)        // print nodes data
      { 
        Serial.println(nodePayload[k]);   
      }   
      Serial.println("*************************");  

      if(nodePayload[1] == 1)
      {
        activeNodes[i] = -1;
      }  
      if(nodePayload[2] == 1)
      {
        lives--;
      }
      
      Serial.println("Available Node count:");  
      Serial.println(availableNodeCount);    
      Serial.println("******LIVES*********"); 
      Serial.println(lives); 
    }
  }
  delay(NODE_READ_DELAY);
}

void prepare()
{
  Serial.println("Begin Prepare");
  getAvailableNodes();
  Serial.println("Available Nodes:");
  for(int i = 0; i < NODE_MAX; i ++)
  {
    if(availableNodes[i] > 0)
    {
      Serial.println(availableNodes[i]);
    }
  }
  
  getModulesToActivate();
  activateModules();
}

void randomiseModules()
{
  int moduleArray[availableNodeCount];
  for(int i = 0; i < availableNodeCount; i++)
  {
    moduleArray[i] = availableNodes[i];
  }
  for (int i=0; i < availableNodeCount; i++) 
  {
    int n = random(0, availableNodeCount);  // Integer from 0 to availableNodeCount-1
    int temp = moduleArray[n];
    moduleArray[n] =  moduleArray[i];
    moduleArray[i] = temp;
  }
  for (int i = 0; i < modules; i++)
  {
    activeNodes[i] = moduleArray[i];
  }
}

void getModulesToActivate()
{
  getDifficulty();
  randomiseModules();
}

void activateModules()
{
  for(int i = 0; i < modules; i++)
  {
    // Temporary activate module 2
    Wire.beginTransmission(activeNodes[i]); 
    Wire.write(1);               
    Wire.endTransmission();  
  }  
}

void getDifficulty()
{
  switch(difficulty){
    case 1:  
    {
      lives = 3;
      modules = 3;
      time = 180000; // 3 min
    }
    case 2:   
    {
      lives = 3;
      modules = 4;
      time = 180000; // 3 min
    }
    case 3:   
    {
      lives = 3;
      modules = 5;
      time = 240000; // 4 min
    }
    case 4:   
    {
      lives = 3;
      modules = 6;
      time = 240000; // 4 min
    }
  }
  if(modules > availableNodeCount)
  {
    modules = availableNodeCount;
  }
}

void getAvailableNodes()
{
  Serial.println("Begin get available nodes");
  for (int nodeAddress = START_NODE; nodeAddress <= NODE_MAX; nodeAddress++) // we are starting from Node address 2
  { 
    Wire.requestFrom(nodeAddress, PAYLOAD_SIZE);    // request data from node#
    if(Wire.available() == PAYLOAD_SIZE)            // if data size is avaliable from nodes
    {
      for (int i = 0; i < PAYLOAD_SIZE; i++)        // get nodes data
      {
        nodePayload[i] = Wire.read(); 
      }  
      for (int j = 0; j < PAYLOAD_SIZE; j++)        // print nodes data
      { 
        Serial.println(nodePayload[j]);   
      }   
      Serial.println("*************************");  
      availableNodes[availableNodeCount] = nodePayload[0];  // add node to availableNodes  
      availableNodeCount++; // increase node count index    
      
      Serial.println("Available Node count:");  
      Serial.println(availableNodeCount);    
    }
  }
  delay(NODE_READ_DELAY);
}

void isr()   // Interrupt Routine
{
  if(booted && !start)
  {
    ready = true;
    Serial.println("Ready");
    booted = false;
  }
}
